package br.com.syonet.business;

import java.util.List;
import java.util.Objects;

import br.com.syonet.dao.AutorDAO;
import br.com.syonet.entidade.Autor;
import br.com.syonet.exception.IdNotExistException;
import br.com.syonet.exception.RecordNullException;

public class AutorBusiness {
	
	private AutorDAO daoAutor = new AutorDAO();

	public void criaAutor( List<Autor> registroAutor ) throws RecordNullException {
		if( Objects.isNull( registroAutor ) ) {
			throw new RecordNullException( "Registro do Autor est� nulo!" );
		}
		if( Objects.isNull(registroAutor.get(0) ) ) {
			throw new RecordNullException( "Nome do autor est� nulo!" ); 
		}
		this.daoAutor.criaAutor(registroAutor);
	}
	
	public Autor consultaIdAutor( Integer id ) throws IdNotExistException {
		if( id == null ) {
			throw new IdNotExistException( "Id n�o encontrado!" );
		}
		
	return this.daoAutor.consultaIdAutor(id);
}
}
