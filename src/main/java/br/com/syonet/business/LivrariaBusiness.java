package br.com.syonet.business;

import java.util.List;
import java.util.Objects;

import br.com.syonet.dao.LivroDAO;
import br.com.syonet.entidade.Livro;
import br.com.syonet.exception.AuthorNotExistException;
import br.com.syonet.exception.IdNotExistException;
import br.com.syonet.exception.RecordNullException;

public class LivrariaBusiness {

	private LivroDAO dao = new LivroDAO();
	
		
	public void criaLivro( List<Livro> registroLivro ) throws RecordNullException {
		if( Objects.isNull( registroLivro ) ) {
			throw new RecordNullException( "Registro do Livro est� nulo!" );
		}
		if( Objects.isNull( registroLivro.get(0) ) ) {
			throw new RecordNullException( "T�tulo do livro est� nulo!" );
		}
		if( Objects.isNull(registroLivro.get(0) ) ) {
			throw new RecordNullException( "Nome do autor est� nulo!" ); 
		}
		this.dao.criaLivro(registroLivro);
	}
	
	public void consultaId( Integer id ) throws IdNotExistException {
		if( id == null ) {
			throw new IdNotExistException( "Id n�o encontrado!" );
		}
		this.dao.consultaId(id);
	}
	
}
