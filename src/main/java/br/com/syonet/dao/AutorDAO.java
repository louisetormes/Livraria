package br.com.syonet.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.syonet.entidade.Autor;

public class AutorDAO {

	public void criaAutor( List<Autor> registroAutor ) {
		for(Autor autor:registroAutor) {
			EntityManagerFactory  entityManagerFactory = Persistence
					.createEntityManagerFactory("dblivraria");
					EntityManager entityManager = entityManagerFactory.createEntityManager();

					entityManager.getTransaction().begin();
					entityManager.persist(autor);
					entityManager.getTransaction().commit();

					entityManager.close();
					entityManagerFactory.close();
		}
	}
		
		 
		public Autor consultaIdAutor( Integer idAutor ) {
			EntityManagerFactory entityManagerFactory = Persistence
					.createEntityManagerFactory("dblivraria");
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			
			entityManager.getTransaction().begin();
			Autor autor = entityManager.find(Autor.class, idAutor);
			
			entityManager.close();
			entityManagerFactory.close();
			
			return autor;
		}
}

