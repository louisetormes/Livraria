package br.com.syonet.dao;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.syonet.entidade.Autor;
import br.com.syonet.entidade.Livro;

public class LivroDAO {

	public void criaLivro( List<Livro> registroLivro ) {
		for(Livro livro:registroLivro) {
		EntityManagerFactory  entityManagerFactory = Persistence
				.createEntityManagerFactory("dblivraria");
				EntityManager entityManager = entityManagerFactory.createEntityManager();

				entityManager.getTransaction().begin();
				entityManager.persist(registroLivro);
				entityManager.getTransaction().commit();

				entityManager.close();
				entityManagerFactory.close();
		}
}
	
	public Livro consultaId( Integer idLivro ) {
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("dblivraria");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		entityManager.getTransaction().begin();
		Livro livro = entityManager.find(Livro.class, idLivro);
		
		entityManager.close();
		entityManagerFactory.close();
		
		return livro;
	}
	
}
