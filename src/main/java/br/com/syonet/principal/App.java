package br.com.syonet.principal;

import java.util.Arrays;
import java.util.List;

import br.com.syonet.business.AutorBusiness;
import br.com.syonet.business.LivrariaBusiness;
import br.com.syonet.entidade.Autor;
import br.com.syonet.entidade.Livro;
import br.com.syonet.exception.AuthorNotExistException;
import br.com.syonet.exception.IdNotExistException;
import br.com.syonet.exception.RecordNullException;

public class App {

	public static void main(String[] args) throws RecordNullException, IdNotExistException, AuthorNotExistException {
//		List<Autor> listaAutor = Arrays.asList(
//				new Autor("John", "Green", "15/10/1990", "New York City"),
//				new Autor("Louise", "Tormes", "04/05/1996", "Estancia Velha"));
		AutorBusiness autorB = new AutorBusiness();
		Autor autor1 = autorB.consultaIdAutor(1);
		
//		autorB.criaAutor(autor1);
//		System.out.println("Autor criado com sucesso!");
		
		List<Livro> listaLivro = Arrays.asList(
				new Livro("A Culpa � das Estrelas", "A Culpa � das Estrelas", Arrays.asList(autor1), "Intrinseca", 364, 2),
				new Livro("Quem � voc� Alasca?", "Quem � voc� Alasca", Arrays.asList(autor1), "Intrinseca", 450, 1));

		LivrariaBusiness business = new LivrariaBusiness();
		business.criaLivro(Arrays.asList(autor1));
		System.out.println("Livro criado no banco!");
		
		
		Livro idLivro = new Livro();
		idLivro.setId(1);
		
		business.consultaId(1);
		System.out.println("Livro com o id encontrado:");
		
	}
}
